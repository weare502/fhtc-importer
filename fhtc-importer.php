<?php

use voku\helper\UTF8;

/**
 * Plugin Name: FHTC JSON Importer
 * Author: We are 502
 * Version: 1.2.1
 * Description: DO NOT DEACTIVATE OR DELETE! Creates the admin menu to set advising and course JSON file URLs. Manages the CRON task for updating courses.
 */

require 'vendor/autoload.php';

add_filter( 'cron_schedules', function(){
    $schedules['fhtc_employee_import_processing'] = array(
        'interval' => 300,
        'display' => esc_html__( 'Every Five Minutes / Employee Import Processing' ),
    );
    return $schedules;
});

// Courses hook
if ( ! wp_next_scheduled( 'fhtc_course_importer_cron_hook' ) ) {
    wp_schedule_event( time(), 'hourly', 'fhtc_course_importer_cron_hook' );
}

// Employees hook
if ( ! wp_next_scheduled( 'fhtc_employee_importer_cron_hook' ) ) {
    wp_schedule_event( time(), 'hourly', 'fhtc_employee_importer_cron_hook' );
}

// Employee processing hook
if ( ! wp_next_scheduled( 'fhtc_employee_importer_processing_cron_hook' ) ) {
    wp_schedule_event( time(), 'fhtc_employee_import_processing', 'fhtc_employee_importer_processing_cron_hook' );
}

register_deactivation_hook( __FILE__, 'fhtc_course_importer_cron_hook_deactivate' );
 
function fhtc_course_importer_cron_hook_deactivate() {
   $timestamp = wp_next_scheduled( 'fhtc_course_importer_cron_hook' );
   wp_unschedule_event( $timestamp, 'fhtc_course_importer_cron_hook' );

   $timestamp = wp_next_scheduled( 'fhtc_employee_importer_cron_hook' );
   wp_unschedule_event( $timestamp, 'fhtc_employee_importer_cron_hook' );

   $timestamp = wp_next_scheduled( 'fhtc_employee_importer_processing_cron_hook' );
   wp_unschedule_event( $timestamp, 'fhtc_employee_importer_processing_cron_hook' );
}

add_action('fhtc_course_importer_cron_hook', 'fhtc_course_importer_cron_hook_callback');

function fhtc_course_importer_cron_hook_callback(){
    $url = get_option('course_import_json_url');

    fhtc_process_course_imports( $url );
}

add_action('fhtc_employee_importer_cron_hook', function(){
    $url = get_option('employee_import_trigger_url');
    $args = array(
        'timeout' => '30',
        'blocking' => true,
        'sslverify' => false,
    );
    if ( $url ){
        $response = wp_remote_get( $url, $args );
    }
});

add_action('fhtc_employee_importer_processing_cron_hook', function(){
    $url = get_option('employee_import_processing_url');
    $args = array(
        'timeout' => '30',
        'blocking' => true,
        'sslverify' => false,
    );
    if ( $url ){
        $response = wp_remote_get( $url, $args );
    }
});

function fhtc_process_course_imports( $url ){
    $json = json_decode( UTF8::file_get_contents($url), true );

    if ( $json === null ){
        wp_die('Invalid JSON file format. Go back and try again. If this problem persists, the file may not be correctly formatted.');
    }

    $old_posts = get_posts(['post_type' => 'course', 'posts_per_page' => -1]);

    if ( $old_posts ){
        foreach ( $old_posts as $p ){
            wp_delete_post($p->ID);
        }
    }

    $courses = [];

    foreach ($json['Courses'] as $section){
        $arc = trim($section['ARC']);
	
        if ( isset( $courses[ $arc ] ) ){
            $courses[$arc][] = $section;
        } else {
            $courses[$arc] = [$section];
        }    
    }

    foreach ($courses as $key => $course ){
        $id = wp_insert_post( ['post_title' => $course[0]['ARC'], 'post_type' => 'course', 'post_status' => 'publish'] );
        update_post_meta($id, 'course_info', $course);
        update_post_meta($id, 'arc', $key);
    }
}

function fhtc_process_advising_data( $url ){
    $json = json_decode( UTF8::file_get_contents($url), true );

    if ( $json === null ){

        wp_die('Invalid JSON file format. Go back and try again. If this problem persists, the file may not be correctly formatted.' . json_last_error());
    }

    if (! isset($json['AdvisingData']) ){
        wp_die('Invalid JSON data found. The consistent markert that we look for was not found. This data has not been imported. Please go back and try a different file or contact 502 to confirm the file format.');
    }

    update_option('advising_data', $json, false);
}


add_action('admin_init', function(){
    if ( isset($_REQUEST['json_file_url']) && ! empty($_REQUEST['json_file_url']) ){
        check_admin_referer('fhtc_import_course_data');

        $url = esc_url_raw(trim($_REQUEST['json_file_url']));

        update_option('course_import_json_url', $url, false);

        fhtc_process_course_imports( $url );
    }

    if ( isset($_REQUEST['advising_json_file_url']) && ! empty($_REQUEST['advising_json_file_url']) ){
        check_admin_referer('fhtc_import_advising_data');

        $url = esc_url_raw(trim($_REQUEST['advising_json_file_url']));

        update_option('advising_import_json_url', $url, false);

        fhtc_process_advising_data( $url );
    }

    if ( isset($_REQUEST['employee_import_trigger_url']) && ! empty($_REQUEST['employee_import_trigger_url']) ){
        check_admin_referer('fhtc_employee_urls');

        $url = esc_url_raw(trim($_REQUEST['employee_import_trigger_url']));

        update_option('employee_import_trigger_url', $url, false);

        $url = esc_url_raw(trim($_REQUEST['employee_import_processing_url']));

        update_option('employee_import_processing_url', $url, false);
    }
});

add_action('admin_menu', function(){
    add_menu_page('FHTC Course & Advising Imports', 'Imp. Crs / Advs Data', 'edit_posts', 'fhtc-import-course-data', function(){ ?>
        <div class="wrap">
            <h1>Import Course Data</h1>
            <?php if ( isset($_REQUEST['updated']) && $_REQUEST['updated'] == '1' ) : ?>
                <div style="padding: 1rem; color: green; font-weight: bold; background: white;">Courses Updated</div>
            <?php endif; ?>
            <form action="<?php echo add_query_arg('updated', 1); ?>" method="POST">
                <?php wp_nonce_field('fhtc_import_course_data'); ?>
                <p>The full URL of the JSON file to import the course data from. <br>
                    This file may be uploaded to the Media Library or hosted on a remote server that is publicly accessible over the internet.
                    <br>
                    Example: <code>https://mysite.com/myfile.json</code></p>
                <label for="json_file_url">Course JSON File URL</label>
                <div><input required class="regular-text" type="text" id="json_file_url" name="json_file_url" value="<?php echo get_option('course_import_json_url', ''); ?>"></div>
                <button type="submit" class="button button-primary" style="margin-top: 2rem;">Run Importer</button>
            </form>

            <hr style="margin-top: 2rem;">

            <h1>Import Advising Data</h1>

            <?php if ( isset($_REQUEST['updated_advising']) && $_REQUEST['updated_advising'] == '1' ) : ?>
                <div style="padding: 1rem; color: green; font-weight: bold; background: white;">Advising Data Updated</div>
            <?php endif; ?>

            <form action="<?php echo add_query_arg('updated_advising', 1); ?>" method="POST">
                <?php wp_nonce_field('fhtc_import_advising_data'); ?>
                <p>The full URL of the JSON file to import the advising data from. <br>
                    This file may be uploaded to the Media Library or hosted on a remote server that is publicly accessible over the internet.
                    <br>
                    Example: <code>https://mysite.com/myfile.json</code></p>
                <label for="advising_json_file_url">Advising JSON File URL</label>
                <div><input required class="regular-text" type="text" id="advising_json_file_url" name="advising_json_file_url" value="<?php echo get_option('advising_import_json_url', ''); ?>"></div>
                <button type="submit" class="button button-primary" style="margin-top: 2rem;">Run Importer</button>
            </form>

            <hr style="margin-top: 2rem;">

            <h1>Employee Data Import URLs</h1>

            <?php if ( isset($_REQUEST['updated_employees']) && $_REQUEST['updated_employees'] == '1' ) : ?>
                <div style="padding: 1rem; color: green; font-weight: bold; background: white;">Employee Trigger URLs Updated</div>
            <?php endif; ?>

            <form action="<?php echo add_query_arg('updated_employees', 1); ?>" method="POST">
                <?php wp_nonce_field('fhtc_employee_urls'); ?>
                <p>The full URL of the of the manual processing links from WP All Import <br> This will only run once an hour. You can manually trigger it from the WP All Import plugin settings.</p>
                <label for="employee_import_trigger_url">Employee Import Trigger URL</label>
                <div><input required class="regular-text" type="text" id="employee_import_trigger_url" name="employee_import_trigger_url" value="<?php echo get_option('employee_import_trigger_url', ''); ?>"></div>
                <label for="employee_import_processing_url">Employee Import Processing URL</label>
                <div><input required class="regular-text" type="text" id="employee_import_processing_url" name="employee_import_processing_url" value="<?php echo get_option('employee_import_processing_url', ''); ?>"></div>
                <button type="submit" class="button button-primary" style="margin-top: 2rem;">Save Employee Import URLs</button>
            </form>
        </div>
    <?php });
});

function fhtc_get_program_from_advising_data( $program_slug ){
    $json = get_option('advising_data', false);

    if ( ! $json || ! isset($json['AdvisingData']) ){
        return false;
    }

	$programs = $json['AdvisingData'];

	$years = [];

	foreach ( $programs as $program ){
		if ( trim($program['Advising']['Program']) === trim($program_slug) ){
			return $program;
		}
	}
}

function fhtc_get_program_type_data( $program_data, $type ){
	if ( ! isset($program_data['Advising'] ) )
		return null; // data likely invalid

	foreach ($program_data['ProgramData'] as $pd){
		if ( $pd['Program']['Type'] === $type ) 
			return $pd['AimData'];
	}
}